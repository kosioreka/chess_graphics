﻿using Chess_graphics.Models;
using Chess_graphics.Models.BasicModels;
using Chess_graphics.Transformations;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using ObjLoader.Loader.Data.VertexData;
using ObjLoader.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Loader
{
    public class ObjectReader
    {
        private readonly IObjLoader _loader;

        public ObjectReader()
        {
            var factory = new ObjLoaderFactory();
            _loader = factory.Create();
        }

        public LoadResult Load(string path)
        {
            try
            {
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    return _loader.Load(stream);
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Triangle> LoadPiece(PieceType name)
        {
            List<Triangle> Triangles = new List<Triangle>();
            LoadResult result = Load("./PieceModels/" + name.ToString() + ".obj");
            foreach (var group in result.Groups)
            {
                foreach (var face in group.Faces)
                {
                    List<Models.BasicModels.Vertex> vertices = new List<Models.BasicModels.Vertex>();
                    for (int i = 0; i < face.Count; i++)
                    {
                        var vertex = result.Vertices[face[i].VertexIndex - 1];
                        var normal = result.Normals[face[i].NormalIndex - 1];
                        Point3D point = new Point3D(vertex.X, vertex.Y, vertex.Z);
                        Vector3D vector = new Vector3D(normal.X, normal.Y, normal.Z);
                        vector.Normalize();
                        Models.BasicModels.Vertex v = new Models.BasicModels.Vertex(point, vector);
                        vertices.Add(v);
                    }
                    if (vertices.Count != 3) throw new InvalidOperationException();
                    Triangle triangle = new Triangle(vertices[0], vertices[1], vertices[2]);
                    Triangles.Add(triangle);
                }
            }
            return Triangles;
        }


    }
}
