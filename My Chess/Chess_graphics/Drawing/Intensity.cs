﻿using Chess_graphics.Models.BasicModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Drawing
{
    public class Intensity
    {
        public double R;
        public double G;
        public double B;

        public Intensity(double r, double g, double b)
        {
            R = r;
            G = g;
            B = b;
        }

        public static Intensity operator +(Intensity intensity1, Intensity intensity2)
        {
            return new Intensity(intensity1.R + intensity2.R, intensity1.G + intensity2.G, intensity1.B + intensity2.B);
        }
    }

    public class IntensityVertex
    {
        public Vertex Coordinates { get; set; }
        public Vertex WorldCoordinates { get; set; }
        public Intensity Intensity { get; set; }

        public IntensityVertex(Vertex coordinates, Vertex worldCoordinates, Intensity intensity)
        {
            Coordinates = coordinates;
            WorldCoordinates = worldCoordinates;
            Intensity = intensity;
        }
    }
}
