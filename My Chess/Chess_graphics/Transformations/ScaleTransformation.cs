﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class ScaleTransform : MatrixTransformation
    {
        public ScaleTransform(double scaleX, double scaleY, double scaleZ)
        {
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {scaleX, 0, 0, 0},
                {0, scaleY, 0, 0},
                {0, 0, scaleZ, 0},
                {0, 0, 0, 1}
            });
        }
    }
}
