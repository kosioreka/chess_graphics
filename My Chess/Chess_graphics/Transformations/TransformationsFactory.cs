﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    public class TransformationsFactory
    {
        public ITransformation CreateMatrixTransformation(Matrix<double> matrix)
        {
            return new MatrixTransformation(matrix);
        }

        public ITransformation CreateScaleTransformation(double scaleX, double scaleY, double scaleZ)
        {
            return new ScaleTransform(scaleX,scaleY, scaleZ);
        }

        public ITransformation CreateTranslationTransformation(Vector<double> vector)
        {
            return new TranslationTransformation(vector);
        }

        public ITransformation CreateRotationXTransformation(double angleInDegrees)
        {
            return new RotationXTransformation(angleInDegrees);
        }

        public ITransformation CreateRotationYTransformation(double angleInDegrees)
        {
            return new RotationYTransformation(angleInDegrees);
        }

        public ITransformation CreateRotationZTransformation(double angleInDegrees)
        {
            return new RotationZTransformation(angleInDegrees);
        }

        public ITransformation CreateRotationTransformation(Vector<double> line, double angleInDegrees)
        {
            return new RotationTransformation(line, angleInDegrees);
        }

        public ITransformation CreateCompositionTransformation(params ITransformation[] transformations)
        {
            Matrix<double> matrix = Matrix<double>.Build.DenseIdentity(4);
            for (int i = transformations.Length - 1; i >= 0; i--)
                matrix = transformations[i].Matrix*matrix;
            return new MatrixTransformation(matrix);
        }

        public ITransformation CreateIdentityTransformation()
        {
            return CreateMatrixTransformation(Matrix<double>.Build.DenseIdentity(4));
        }
    }
}
