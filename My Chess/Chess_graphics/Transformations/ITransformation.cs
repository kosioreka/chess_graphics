﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using Chess_graphics.Models;

namespace Chess.Models.Transformations
{
    public interface ITransformation
    {
        Vector<double> Transform(Vector<double> oint);
        Matrix<double> Matrix { get; }
    }

    public static class TransformationExtensions
    {
        public static Vector<double> TransformVector(this ITransformation transformation, Vector<double> vector)
        {
            return transformation.Transform(vector);
        }

        public static void TransformModel(this PieceModel model, ITransformation transformation)
        {
            var matrix = transformation.Matrix * model.ModelMatrix;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    model.ModelMatrix[i, j] = matrix[i, j];
                }
            }
        }

        public static ITransformation Combine(this ITransformation transformation, ITransformation transformation2)
        {
            return
                new TransformationsFactory().CreateCompositionTransformation(new ITransformation[]
                {transformation, transformation2});
        }
    }
}
