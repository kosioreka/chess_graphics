﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class MatrixTransformation : ITransformation
    {
        public Matrix<double> Matrix { get; protected set; }

        protected MatrixTransformation() { }

        public MatrixTransformation(Matrix<double> matrix)
        {
            if (matrix == null) throw new ArgumentNullException(nameof(matrix));
            if (matrix.RowCount != 4 && matrix.ColumnCount != 4)
                throw new ArgumentException(@"Invalid matrix dimensions");
            Matrix = matrix;
        }

        public Vector<double> Transform(Vector<double> point)
        {
            return Matrix * (Vector<double>)point;
        }
    }
}
