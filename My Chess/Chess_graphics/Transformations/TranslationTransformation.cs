﻿using System;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class TranslationTransformation : MatrixTransformation
    {
        public TranslationTransformation(Vector<double> vector) : base()
        {
            if(vector==null) throw new ArgumentNullException(nameof(vector));
            Vector = vector;
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {1, 0, 0, vector[0]},
                {0, 1, 0, vector[1]},
                {0, 0, 1, vector[2]},
                {0, 0, 0, 1}
            });
        }

        public Vector<double> Vector { get; }
    }
}