﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class RotationTransformation : MatrixTransformation
    {
        public RotationTransformation(Vector<double> line, double angleInDegrees)
        {
            if(line==null) throw new ArgumentNullException(nameof(line));
            Line = line;
            AngleInDegrees = angleInDegrees;
            double angle = angleInDegrees*Math.PI/180;
            double s = Math.Sin(angle);
            double c = Math.Cos(angle);
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                { line[0]*line[0]+(1-line[0])*(1-line[0])*c, line[0]*line[1]*(1-c)-line[2]*s, line[0]*line[2]*(1-c)+line[1]*s, 0},
                { line[1]*line[0]*(1-c)+line[2]*s, line[1]*line[1]+(1-line[1])*(1-line[1])*c, line[1]*line[2]*(1-c)-line[0]*s,0},
                { line[0]*line[2]*(1-c)-line[1]*s, line[1]*line[2]*(1-c)+line[0]*s, line[2]*line[2] + (1-line[2])*(1-line[2])*c,0},
                {0,0,0,1 }
            });
        }

        protected RotationTransformation() { }

        public Vector<double> Line { get; protected set; }
        public double AngleInDegrees { get; protected set; }
    }
}
