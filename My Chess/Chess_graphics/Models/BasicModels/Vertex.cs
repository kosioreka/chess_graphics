﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.BasicModels
{

    public class Vertex
    {
        public Point3D Point { get; set; }
        public Vector3D Normal { get; set; }

        public Vertex(Point3D point, Vector3D normal)
        {
            Point = point;
            Normal = normal;
        }
    }

}
