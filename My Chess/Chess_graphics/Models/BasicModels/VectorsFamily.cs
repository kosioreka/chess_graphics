﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.BasicModels
{
    public class VectorsFamily
    {
        public Vector<double> vector;
        public double X { get { return vector[0]; } set { vector[0] = value; } }
        public double Y { get { return vector[1]; } set { vector[1] = value; } }
        public double Z { get { return vector[2]; } set { vector[2] = value; } }
        public double W { get { return vector[3]; } set { vector[3] = value; } }

        public void Normalize()
        {
            double length = Length();
            if (Math.Abs(length) <= 0) return;
            X = X / length;
            Y = Y / length;
            Z = Z / length;
            W = W / length;
        }
        public double Length()
        {
            return Math.Sqrt(X * X + Y * Y + Z * Z);
        }
        public double Length2()
        {
            return Math.Sqrt(X * X + Y * Y + Z * Z + W * W);
        }
        public void Normalize2()
        {
            double l = W;
            X /= l;
            Y /= l;
            Z /= l;
            W /= l;
        }
    }
}
