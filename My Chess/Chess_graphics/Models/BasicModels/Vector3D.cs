﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.BasicModels
{
    public class Vector3D : VectorsFamily
    {
        public Vector3D() : this(0, 0, 0) { }

        public Vector3D(double x, double y, double z, double w = 0) : this(new double[] { x, y, z, w }) { }

        public Vector3D(double[] point)
        {
            vector = Vector<double>.Build.DenseOfArray(point);
        }
        public Vector3D(Vector<double> _vector)
        {
            vector = _vector;
        }

        public Vector3D Clone()
        {
            return new Vector3D(X, Y, Z, W);
        }
        public Vector3D Symmetry(Vector3D normal)
        {
            return new Vector3D((2 * (DotProduct(normal)) * normal.vector) - vector);
        }
        public double DotProduct(Vector3D vector)
        {
            return X * vector.X + Y * vector.Y + Z * vector.Z;
        }
        public double AngleCos(Vector3D vector)
        {
            return DotProduct(vector) / Length() / vector.Length();
        }
        public Vector3D CrossProduct( Vector3D vector)
        {
            return new Vector3D(Y * vector.Z - Z * vector.Y, Z * vector.X - X * vector.Z, X * vector.Y - vector.X * Y);
        }
    }
}
