﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.BasicModels
{
    public class Point3D : VectorsFamily
    {
        public Point3D() : this(0, 0, 0) { }

        public Point3D(double x, double y, double z, double w = 1) : this(new double[] { x, y, z, w }) { }

        public Point3D(double [] point)
        {
            vector = Vector<double>.Build.DenseOfArray(point);
        }
        public Point3D(Vector<double> _vector)
        {
            vector = _vector;
        }   

        public Point3D Clone()
        {
            return new Point3D(X, Y, Z, W);
        }
    }
}
