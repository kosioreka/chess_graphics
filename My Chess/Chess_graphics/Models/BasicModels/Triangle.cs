﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.BasicModels
{
    public class Triangle
    {
        public Vertex[] Vertices { get; }

        public Triangle(Vertex a, Vertex b, Vertex c)
        {
            Vertices = new[] { a, b, c };
        }
    }
}
