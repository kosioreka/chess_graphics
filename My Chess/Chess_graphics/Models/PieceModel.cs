﻿using Chess_graphics.Drawing;
using Chess_graphics.Managers;
using Chess_graphics.Models.BasicModels;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models
{
    public class PieceModel
    {
        public List<Triangle> Triangles { get; set; }
        public Matrix<double> ModelMatrix { get; set; }
        public PieceType Type { get; set; }
        public PieceColor ColorType { get; set; }
        public Color BaseColor { get; set; }
        public IlluminationProperties IlluminationProperties { get; set; }

        public PieceModel(PieceType type, PieceColor color) : this(type,color, Matrix<double>.Build.DenseIdentity(4)) { }

        public PieceModel(PieceType type, PieceColor color, Matrix<double> modelMatrix, IlluminationProperties properties = null)
        {
            Type = type;
            ColorType = color;
            setColor(color);
            ModelMatrix = modelMatrix;
            IlluminationProperties = properties ?? new IlluminationProperties()
            {
                AmbientReflections = new IntensityCoefficients() { R = 0.5, G = 0.5, B = 0.5 },
                DiffuseReflections = new IntensityCoefficients() { R = 0.95, G = 0.95, B = 0.95 },
                SpecularReflections = new IntensityCoefficients() { R = 0.6, G = 0.6, B = 0.6 },
                NShine = 10
            };
            Triangles = new List<Triangle>();
        }

        public void ResetModelMatrix()
        {
            var matrix = ModelMatrix;
            var inv = matrix.Inverse().Transpose();
            foreach (var triangle in Triangles)
            {
                foreach (var vertex in triangle.Vertices)
                {
                    vertex.Point.vector = matrix * vertex.Point.vector;
                    vertex.Normal.vector = inv * vertex.Normal.vector;
                    vertex.Normal.Normalize();
                    vertex.Normal.W = 0;
                }
            }
            ModelMatrix = Matrix<double>.Build.DenseIdentity(4);
        }

        public void setColor(PieceColor color)
        {
            switch (color)
            {
                case PieceColor.Black:
                    this.BaseColor = Color.FromArgb(50,50,50);
                    return;
                case PieceColor.White:
                    this.BaseColor = Color.White;
                    return;
            }
        }
    }

    public class KnightModel : PieceModel
    {
        public KnightModel(PieceColor color) : base(PieceType.Knight, color) { Direction = MovementDirection.Up; }
        public KnightModel(PieceColor color, Matrix<double> matrix) : base(PieceType.Knight, color, matrix)
        {
            Direction = MovementDirection.Up;
        }

        public MovementDirection Direction { get; set; }
    }

    public enum PieceType
    {
        Pawn,
        Knight,
        Bishop,
        Rook,
        Queen,
        King
    }
    public enum PieceColor
    {
        White,
        Black
    }

}
