﻿using Chess_graphics.Drawing;
using Chess_graphics.Models.BasicModels;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.Shading
{
    public class GouraudShadingModel : ShadingModel
    {

        protected override void FillModel(PieceModel triangleModel, ref Bitmap bitmap)
        {
            Matrix<double> transformMatrix = (ProjectionMatrix * (ViewMatrix * triangleModel.ModelMatrix));
            foreach (var triangle in triangleModel.Triangles)
            {
                Triangle triangle1 = DrawerHelper.TransformTriangle(triangle, W, H, transformMatrix);
                IntensityVertex[] vertices = PrepareVetices(triangle1, triangle, triangleModel.ModelMatrix,
                    triangleModel.IlluminationProperties);
                FillTriangle(vertices[0], vertices[1], vertices[2], triangleModel.BaseColor, ref bitmap, triangleModel.IlluminationProperties);
            }
        }

        protected void FillTriangle(IntensityVertex v1, IntensityVertex v2, IntensityVertex v3, Color color, ref Bitmap bitmap, IlluminationProperties properties = null)
        {
            DrawerHelper.SortVerticesByY(ref v1, ref v2, ref v3);
            double dP1P2, dP1P3;
            var p1 = v1.Coordinates;
            var p2 = v2.Coordinates;
            var p3 = v3.Coordinates;
            DrawerHelper.CalculateCoefficients(p1.Point.X, p1.Point.Y, p2.Point.X, p2.Point.Y,
                p3.Point.X, p3.Point.Y, out dP1P2, out dP1P3);
            if (dP1P2 > dP1P3)
            {
                for (var y = (int)p1.Point.Y; y <= (int)p3.Point.Y; y++)
                {
                    if (y < p2.Point.Y)
                    {
                        ProcessScanLine(y, v1, v3, v1, v2, color, ref bitmap, properties);
                    }
                    else
                    {
                        ProcessScanLine(y, v1, v3, v2, v3, color, ref bitmap, properties);
                    }
                }
            }
            else
            {
                for (var y = (int)p1.Point.Y; y <= (int)p3.Point.Y; y++)
                {
                    if (y < p2.Point.Y)
                    {
                        ProcessScanLine(y, v1, v2, v1, v3, color, ref bitmap, properties);
                    }
                    else
                    {
                        ProcessScanLine(y, v2, v3, v1, v3, color, ref bitmap, properties);
                    }
                }
            }
        }

        protected virtual void ProcessScanLine(int currentY, IntensityVertex va, IntensityVertex vb, IntensityVertex vc, IntensityVertex vd, Color color, ref Bitmap bitmap, IlluminationProperties properties = null)
        {
            Vertex pa = va.Coordinates;
            Vertex pb = vb.Coordinates;
            Vertex pc = vc.Coordinates;
            Vertex pd = vd.Coordinates;

            var gradient1 = (int)pa.Point.Y != (int)pb.Point.Y ? (currentY - pa.Point.Y) / (pb.Point.Y - pa.Point.Y) : 1;
            var gradient2 = (int)pc.Point.Y != (int)pd.Point.Y ? (currentY - pc.Point.Y) / (pd.Point.Y - pc.Point.Y) : 1;

            int sx = (int)DrawerHelper.Interpolate(pa.Point.X, pb.Point.X, gradient1);
            int ex = (int)DrawerHelper.Interpolate(pc.Point.X, pd.Point.X, gradient2);

            double z1 = DrawerHelper.Interpolate(pa.Point.Z, pb.Point.Z, gradient1);
            double z2 = DrawerHelper.Interpolate(pc.Point.Z, pd.Point.Z, gradient2);

            var snl = DrawerHelper.Interpolate(va.Intensity, vb.Intensity, gradient1);
            var enl = DrawerHelper.Interpolate(vc.Intensity, vd.Intensity, gradient2);

            for (var x = sx; x < ex; x++)
            {
                double gradient = (x - sx) / (double)(ex - sx);
                var z = DrawerHelper.Interpolate(z1, z2, gradient);
                Intensity intensity = DrawerHelper.Interpolate(snl, enl, gradient);
                Color col = Color.FromArgb(
                    255,
                    (byte)(Math.Min(255, intensity.R * color.R)),
                    (byte)(Math.Min(255, intensity.G * color.G)),
                    (byte)(Math.Min(255, intensity.B * color.B))
                    );
                // changing the color value using the cosine of the angle
                // between the light vector and the normal vector
                DrawPoint(new Point3D(x, currentY, z), col, ref bitmap);
            }
        }
        private IntensityVertex[] PrepareVetices(Triangle triangle1, Triangle triangle, Matrix<double> modelMatrix, IlluminationProperties properties)
        {
            IntensityVertex[] vertices = new IntensityVertex[3];
            var inv = modelMatrix.Inverse().Transpose();
            for (int i = 0; i < 3; i++)
            {
                Point3D point = new Point3D(modelMatrix * triangle.Vertices[i].Point.vector);
                Vector3D normal = new Vector3D(inv * triangle.Vertices[i].Normal.vector);
                normal.Normalize();
                Vertex coord = new Vertex(point, normal);
                Intensity intensity = IlluminationHelper.CalculateIntensity(coord, properties, Lights, Camera);
                vertices[i] = new IntensityVertex(triangle1.Vertices[i], coord, intensity);
            }
            return vertices;
        }

    }
}
