﻿using Chess_graphics.Drawing;
using Chess_graphics.Models.BasicModels;
using Chess_graphics.Models.Projection;
using Chess_graphics.Models.Shading;
using Chess_graphics.Transformations;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using ObjLoader.Loader.Data.VertexData;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models
{
    public class ShadingModel
    {
        protected Matrix<double> ViewMatrix;
        protected Matrix<double> ProjectionMatrix;
        protected int W;
        protected int H;
        protected Camera Camera;
        protected IList<Light> Lights;

        private int _bitmapWidth;
        private int _bitmapHeight;
        private double[,] _zBuffer;

        public void DrawModels(ref Bitmap bitmap, IEnumerable<PieceModel> models, Camera activeCamera, List<Light> lights)
        {
            _zBuffer = new double[bitmap.Width, bitmap.Height];
            for (int i = 0; i < _zBuffer.GetLength(0); i++)
                for (int j = 0; j < _zBuffer.GetLength(1); j++)
                    _zBuffer[i, j] = double.MaxValue;
            _bitmapWidth = bitmap.Width;
            _bitmapHeight = bitmap.Height;
            Camera = activeCamera;
            Lights = lights;
            W = bitmap.Width / 2;
            H = bitmap.Height / 2;
            ViewMatrix = Camera.CreateLookAt();
            ProjectionMatrix = Camera.CreateProjectionView(1, 100, (double)H / W);
            foreach (var triangleModel in models)
            {
                FillModel(triangleModel, ref bitmap);
            }
        }

        protected virtual void FillModel(PieceModel triangleModel, ref Bitmap bitmap)
        {
            Matrix<double> transformMatrix = (ProjectionMatrix * (ViewMatrix * triangleModel.ModelMatrix));
            foreach (var triangle in triangleModel.Triangles)
            {
                Triangle triangle1 = DrawerHelper.TransformTriangle(triangle, W, H, transformMatrix);
                FillTriangle(triangle1.Vertices[0], triangle1.Vertices[1], triangle1.Vertices[2], triangleModel.BaseColor, ref bitmap);
            }
        }

        protected void FillTriangle(BasicModels.Vertex p1, BasicModels.Vertex p2, BasicModels.Vertex p3, Color color, ref Bitmap bitmap)
        {
            //Sorting by Y
            DrawerHelper.SortVerticesByY(ref p1, ref p2, ref p3);
            double dP1P2, dP1P3;
            // computing lines' directions
            DrawerHelper.CalculateCoefficients(p1.Point.X, p1.Point.Y, p2.Point.X, p2.Point.Y, p3.Point.X, p3.Point.Y, out dP1P2, out dP1P3);
            // P1
            // -
            // -- 
            // - -
            // -  -
            // -   - P2
            // -  -
            // - -
            // -
            // P3
            if (dP1P2 > dP1P3)
            {
                for (var y = (int)p1.Point.Y; y <= (int)p3.Point.Y; y++)
                {
                    if (y < p2.Point.Y)
                    {
                        ProcessScanLine(y, p1, p3, p1, p2, color, ref bitmap);
                    }
                    else
                    {
                        ProcessScanLine(y, p1, p3, p2, p3, color, ref bitmap);
                    }
                }
            }
            //       P1
            //        -
            //       -- 
            //      - -
            //     -  -
            // P2 -   - 
            //     -  -
            //      - -
            //        -
            //       P3
            else
            {
                for (var y = (int)p1.Point.Y; y <= (int)p3.Point.Y; y++)
                {
                    if (y < p2.Point.Y)
                    {
                        ProcessScanLine(y, p1, p2, p1, p3, color, ref bitmap);
                    }
                    else
                    {
                        ProcessScanLine(y, p2, p3, p1, p3, color, ref bitmap);
                    }
                }
            }
        }
        /// <summary>
        /// // drawing line between 2 points from left to right
        /// </summary>
        /// <param name="y"></param>
        /// <param name="pa"></param>
        /// <param name="pb"></param>
        /// <param name="pc"></param>
        /// <param name="pd"></param>
        /// <param name="color"></param>
        /// <param name="bitmap"></param>
        protected void ProcessScanLine(int y, BasicModels.Vertex pa, BasicModels.Vertex pb, BasicModels.Vertex pc, BasicModels.Vertex pd, Color color, ref Bitmap bitmap)
        {
            // Thanks to current Y, we can compute the gradient to compute others values like
            // the starting X (sx) and ending X (ex) to draw between
            // if pa.Y == pb.Y or pc.Y == pd.Y, gradient is forced to 1
            var gradient1 = (int)pa.Point.Y != (int)pb.Point.Y ? (y - pa.Point.Y) / (pb.Point.Y - pa.Point.Y) : 1;
            var gradient2 = (int)pc.Point.Y != (int)pd.Point.Y ? (y - pc.Point.Y) / (pd.Point.Y - pc.Point.Y) : 1;
            int sx = (int)DrawerHelper.Interpolate(pa.Point.X, pb.Point.X, gradient1);
            int ex = (int)DrawerHelper.Interpolate(pc.Point.X, pd.Point.X, gradient2);
            // starting Z & ending Z
            double z1 = DrawerHelper.Interpolate(pa.Point.Z, pb.Point.Z, gradient1);
            double z2 = DrawerHelper.Interpolate(pc.Point.Z, pd.Point.Z, gradient2);
            // drawing a line from left (sx) to right (ex)
            for (var x = sx; x < ex; x++)
            {
                double gradient = (x - sx) / (double)(ex - sx);
                var z = DrawerHelper.Interpolate(z1, z2, gradient);
                DrawPoint(new Point3D(x, y, z), color, ref bitmap);
            }
        }

        protected void DrawPoint(Point3D point, Color color, ref Bitmap bitmap)
        {
            if (point.X >= 0 && point.Y >= 0 && point.X < _bitmapWidth && point.Y < _bitmapHeight)
            {
                int x = (int)point.X;
                int y = (int)point.Y;
                //checking with zBuffer
                if (_zBuffer[x, y] < point.Z)
                    return;
                _zBuffer[x, y] = point.Z;

                bitmap.SetPixel((int)point.X, (int)point.Y, color);
            }
        }

    }
    public enum ShadingType { Flat, Gouraud, Phong };

}