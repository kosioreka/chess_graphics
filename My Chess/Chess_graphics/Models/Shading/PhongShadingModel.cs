﻿using Chess_graphics.Drawing;
using Chess_graphics.Models.BasicModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.Shading
{
    public class PhongShadingModel: GouraudShadingModel
    {
        protected override void ProcessScanLine(int currentY, IntensityVertex va, IntensityVertex vb, IntensityVertex vc, IntensityVertex vd,
    Color color, ref Bitmap bitmap, IlluminationProperties properties = null)
        {
            Vertex pa = va.Coordinates;
            Vertex pb = vb.Coordinates;
            Vertex pc = vc.Coordinates;
            Vertex pd = vd.Coordinates;

            var gradient1 = (int)pa.Point.Y != (int)pb.Point.Y ? (currentY - pa.Point.Y) / (pb.Point.Y - pa.Point.Y) : 1;
            var gradient2 = (int)pc.Point.Y != (int)pd.Point.Y ? (currentY - pc.Point.Y) / (pd.Point.Y - pc.Point.Y) : 1;

            int sx = (int)DrawerHelper.Interpolate(pa.Point.X, pb.Point.X, gradient1);
            int ex = (int)DrawerHelper.Interpolate(pc.Point.X, pd.Point.X, gradient2);

            double z1 = DrawerHelper.Interpolate(pa.Point.Z, pb.Point.Z, gradient1);
            double z2 = DrawerHelper.Interpolate(pc.Point.Z, pd.Point.Z, gradient2);

            var snl = DrawerHelper.Interpolate(va.WorldCoordinates.Normal, vb.WorldCoordinates.Normal, gradient1);
            var enl = DrawerHelper.Interpolate(vc.WorldCoordinates.Normal, vd.WorldCoordinates.Normal, gradient2);

            var spl = DrawerHelper.Interpolate(va.WorldCoordinates.Point, vb.WorldCoordinates.Point, gradient1);
            var epl = DrawerHelper.Interpolate(vc.WorldCoordinates.Point, vd.WorldCoordinates.Point, gradient2);

            
            for (var x = sx; x < ex; x++)
            {
                double gradient = (x - sx) / (double)(ex - sx);
                var z = DrawerHelper.Interpolate(z1, z2, gradient);
                Vector3D normal = DrawerHelper.Interpolate(snl, enl, gradient);
                Point3D point = DrawerHelper.Interpolate(spl, epl, gradient);
                Intensity intensity = IlluminationHelper.CalculateIntensity(new Vertex(point, normal), properties, Lights, Camera);
                Color col = Color.FromArgb(
                    255,
                    (byte)(Math.Min(255, intensity.R * color.R)),
                    (byte)(Math.Min(255, intensity.G * color.G)),
                    (byte)(Math.Min(255, intensity.B * color.B))
                );
                DrawPoint(new Point3D(x, currentY, z), col, ref bitmap);
            }
        }
    }
}
