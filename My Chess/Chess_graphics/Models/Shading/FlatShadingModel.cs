﻿using Chess_graphics.Drawing;
using Chess_graphics.Models.BasicModels;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.Shading
{
    public class FlatShadingModel : ShadingModel
    {
        protected override void FillModel(PieceModel triangleModel, ref Bitmap bitmap)
        {
            Matrix<double> transformMatrix = (ProjectionMatrix * (ViewMatrix * triangleModel.ModelMatrix));
            foreach (var triangle in triangleModel.Triangles)
            {
                Triangle triangle1 = DrawerHelper.TransformTriangle(triangle, W, H, transformMatrix, triangleModel.ModelMatrix);
                Vertex position = CalculateMiddlePoint(triangle);
                var m = triangleModel.ModelMatrix.Inverse().Transpose();
                position.Point.vector = triangleModel.ModelMatrix * position.Point.vector;
                position.Normal.vector = m * position.Normal.vector;
                position.Normal.Normalize();
                var intensity = IlluminationHelper.CalculateIntensity(position, triangleModel.IlluminationProperties, Lights, Camera);
                Color color = triangleModel.BaseColor;
                var R = (byte)(Math.Min(255, intensity.R * triangleModel.BaseColor.R));
                var G = (byte)(Math.Min(255, intensity.G * triangleModel.BaseColor.G));
                var B = (byte)(Math.Min(255, intensity.B * triangleModel.BaseColor.B));
                color = Color.FromArgb(R, G, B);
                FillTriangle(triangle1.Vertices[0], triangle1.Vertices[1], triangle1.Vertices[2], color, ref bitmap);
            }
        }

        /// <summary>
        /// Calculating middle point of the triangle
        /// </summary>
        /// <param name="triangle"></param>
        /// <returns></returns>
        private Vertex CalculateMiddlePoint(Triangle triangle)
        {
            Vector3D vector = new Vector3D((triangle.Vertices[0].Normal.vector + triangle.Vertices[1].Normal.vector + triangle.Vertices[2].Normal.vector) / 3);
            vector.Normalize();
            Point3D position = new Point3D((triangle.Vertices[0].Point.vector + triangle.Vertices[1].Point.vector + triangle.Vertices[2].Point.vector) / 3);
            return new Vertex(position, vector);
        }
    }
}
