﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.Projection
{
    public class ProjectionMatrix
    {
        public double N { get; set; }

        public double F { get; set; }

        public double Fov { get; set; }

        public double A { get; set; }

        public Matrix<double> Matrix { get; set; }
    }
}
