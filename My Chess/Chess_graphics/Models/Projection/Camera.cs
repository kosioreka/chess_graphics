﻿using Chess_graphics.Models.BasicModels;
using Chess_graphics.Transformations;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.Projection
{
    public class Camera
    {
        public virtual Point3D CameraPosition { get; set; }
        public virtual Point3D CameraTarget { get; set; }
        public Vector3D UpVector { get; private set; }
        public double FieldOfView { get; private set; }

        public Camera(Point3D cameraPosition, Point3D cameraTarget, Vector3D upVector, double fieldOfView = 90)
        {
            CameraPosition = cameraPosition;
            CameraTarget = cameraTarget;
            UpVector = upVector;
            FieldOfView = fieldOfView;
        }

        public Matrix<double> CreateLookAt()
        {
            Vector3D zAxis = new Vector3D(CameraPosition.vector - CameraTarget.vector);
            zAxis.Normalize();
            Vector3D xAxis = UpVector.CrossProduct(zAxis);
            xAxis.Normalize();
            Vector3D yAxis = zAxis.CrossProduct(xAxis);
            yAxis.Normalize();
            Matrix<double> viewInversed = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {xAxis.X, yAxis.X, zAxis.X, CameraPosition.X},
                {xAxis.Y, yAxis.Y, zAxis.Y, CameraPosition.Y },
                {xAxis.Z, yAxis.Z, zAxis.Z, CameraPosition.Z },
                {0,0,0,1 }
            });
            return viewInversed.Inverse();
        }

        public Matrix<double> CreateProjectionView(double near, double far, double a)
        {
            double e = 1 / Math.Tan(FieldOfView * Math.PI / 360);
            return Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {e,0,0,0 },
                { 0,e/a,0,0 },
                {0,0,-(far+near)/(far-near), -2*far*near/(far-near) },
                {0,0,-1,0 }
            });
        }
    }

    public class ObservableCamera : Camera
    {
        public ObservableCamera(Point3D cameraPosition, Vector3D upVector, PieceModel observedModel, double fieldOfView = 90)
            : base(cameraPosition, new Point3D(0, 0, 0), upVector, fieldOfView)
        {
            ObservedModel = observedModel;
        }
        public PieceModel ObservedModel { get; set; }

        public override Point3D CameraTarget => new Point3D(ObservedModel.ModelMatrix * new Vector3D(ObservedModel.Triangles[0].Vertices[0].Point.vector).vector);
    }

    public class MovingObservableCamera : ObservableCamera
    {
        public MovingObservableCamera(Vector3D observeFrom, Vector3D upVector, PieceModel model, double fieldOfView = 90)
            : base(new Point3D(0, 0, 0, 0), upVector, model, fieldOfView)
        {
            ObserveFrom = observeFrom;
        }

        public Vector3D ObserveFrom { get; set; }

        public override Point3D CameraPosition => new Point3D(CameraTarget.vector - ObserveFrom.vector);
    }
    public class SimpleCamera : Camera
    {
        public SimpleCamera(Point3D cameraPosition, Point3D cameraTarget, Vector3D upVector, double fieldOfView = 90)
            : base(cameraPosition, cameraTarget, upVector, fieldOfView) { }
    }

    public class ViewMatrix
    {
        public Vector<double> CameraPosition { get; set; }

        public Vector<double> CameraTarget { get; set; }

        public Vector<double> UpVector { get; set; }

        public Matrix<double> Matrix { get; set; }

    }
}
