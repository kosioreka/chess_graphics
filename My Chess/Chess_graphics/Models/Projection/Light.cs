﻿using Chess_graphics.Models.BasicModels;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Models.Projection
{
    public class Light
    {
        public Light(Point3D position, double ambient, double diffuse, double specular)
        {
            Position = position;
            AmbientIntensity = ambient;
            DiffuseIntensity = diffuse;
            SpecularIntensity = specular;
        }

        public Point3D Position { get; }
        public double AmbientIntensity { get; }
        public double DiffuseIntensity { get; }
        public double SpecularIntensity { get; }
    }
    public class PointLight : Light
    {
        public PointLight(Point3D position) : base(position, 0.4, 1, 1) { }
    }

}
