﻿using Chess_graphics.Managers;
using Chess_graphics.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chess_graphics
{
    public partial class Form1 : Form
    {
        private SceneEngine sceneEngine;

        public Form1()
        {
            InitializeComponent();
        }
        
        private void Initialize()
        {
            Bitmap bitmap = new Bitmap(sceneBox.Width, sceneBox.Height);
            sceneBox.Image = bitmap;
            this.DoubleBuffered = true;

            sceneEngine = new SceneEngine(bitmap);
            sceneEngine.LoadScene();
            double[,] depthBuffer = new double[sceneBox.Width, sceneBox.Height];
            for (int i = 0; i < sceneBox.Width; i++)
            {
                for (int j = 0; j < sceneBox.Height; j++)
                    depthBuffer[i, j] = double.MaxValue;
            }
            Graphics graphics = Graphics.FromImage(sceneBox.Image);
        }

        private void Redraw()
        {
            Bitmap bitmap = new Bitmap(sceneBox.Width, sceneBox.Height);
            sceneEngine.Draw(ref bitmap);
            Graphics g = Graphics.FromImage(sceneBox.Image);
            g.Clear(Color.AliceBlue);

            sceneBox.Image = bitmap;           
            sceneBox.Refresh();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    sceneEngine.Move(MovementDirection.Up);
                    break;
                case Keys.Down:
                    sceneEngine.Move(MovementDirection.Down);
                    break;
                case Keys.Left:
                    sceneEngine.Move(MovementDirection.Left);
                    break;
                case Keys.Right:
                    sceneEngine.Move(MovementDirection.Right);
                    break;
                case Keys.K:
                    sceneEngine.SelectPiece(PieceType.King);
                    break;
                case Keys.Q:
                    sceneEngine.SelectPiece(PieceType.Queen);
                    break;
                case Keys.R:
                    sceneEngine.SelectPiece(PieceType.Rook);
                    break;
                case Keys.P:
                    sceneEngine.SelectPiece(PieceType.Pawn);
                    break;
                case Keys.N:
                    sceneEngine.SelectPiece(PieceType.Knight);
                    break;
                case Keys.B:
                    sceneEngine.SelectPiece(PieceType.Bishop);
                    break;
                case Keys.F1:
                    sceneEngine.SetCamera(0);
                    break;
                case Keys.F2:
                    sceneEngine.SetCamera(1);
                    break;
                case Keys.F3:
                    sceneEngine.SetCamera(2);
                    break;
                case Keys.Tab:
                    sceneEngine.ChangeShadingModel();
                    break;

            }
            Redraw();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Initialize();
            Redraw();
        }
    }
}
