﻿using Chess_graphics.Managers;
using Chess_graphics.Models;
using Chess_graphics.Models.BasicModels;
using Chess_graphics.Models.Projection;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics
{
    public class SceneEngine
    {
        private PieceManager _pieceManager = new PieceManager();
        private CamerasManager _camerasManager;
        private ShadingManager _shadingManager = new ShadingManager();
        private List<Light> _lights;
        private MovementManager _movementManager;

        public Bitmap SceneBitmap  { get; set; }
        public ShadingModel ShadingModel { get; set; }
        public List<PieceModel> WhitePieces = new List<PieceModel>();
        public List<PieceModel> BlackPieces = new List<PieceModel>();
        public List<PieceModel> AllPieces = null;

        public SceneEngine(Bitmap bitmap)
        {
            SceneBitmap = bitmap;
        }

        public void LoadScene()
        {
            WhitePieces = _pieceManager.LoadPieces(PieceColor.White);
            //BlackPieces = _pieceManager.LoadPieces(PieceColor.Black);
            _pieceManager.SetPieces(WhitePieces, 0);
            //_pieceManager.SetPieces(BlackPieces, -2.5);
            _movementManager = new MovementManager(WhitePieces);
            _camerasManager = new CamerasManager(new Camera[]
            {
                new SimpleCamera(new Point3D(0,2,3),new Point3D(0,0,0),new Vector3D(0,1,0)),
                new ObservableCamera(new Point3D(-4,4,0),new Vector3D(0,1,0), _movementManager.SelectedPiece),
                new MovingObservableCamera(new Vector3D(0,-2,-3), new Vector3D(0,1,0), WhitePieces[0]),
            });
            _lights = new List<Light> { new PointLight(new Point3D(0, 3, 3)) };
            AllPieces = new List<PieceModel>(WhitePieces);
            if (BlackPieces != null || BlackPieces.Count > 0)
                AllPieces.AddRange(BlackPieces);
        }

        public void Draw(ref Bitmap bitmap)
        {
            var drawer = _shadingManager.ActiveShadingModel;
            drawer.DrawModels(ref bitmap, AllPieces, _camerasManager.ActiveCamera, _lights);
        }

        internal void Move(MovementDirection direction)
        {
            _movementManager.MovePiece(direction);
        }

        internal void SelectPiece(PieceType type)
        {
            _movementManager.ChangeSelectedPiece(type);
            _camerasManager.SetMovingPiece(_movementManager.SelectedPiece);
        }

        internal void SetCamera(int index)
        {
            _camerasManager.ChangeActiveCamera(index);
        }

        internal void ChangeShadingModel()
        {
            _shadingManager.ChangeActiveShadingModel();
        }
    }
}
