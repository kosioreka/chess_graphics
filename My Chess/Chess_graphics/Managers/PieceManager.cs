﻿using Chess.Models.Transformations;
using Chess_graphics.Loader;
using Chess_graphics.Models;
using Chess_graphics.Models.BasicModels;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Managers
{
    public class PieceManager
    {
        private ObjectReader _objectLoader = new ObjectReader();
        private readonly ITransformation _transform;
        private readonly TransformationsFactory _transformationsFactory = new TransformationsFactory();


        public PieceManager()
        {
            _transform = _transformationsFactory.CreateRotationXTransformation(-90);
            var scale = _transformationsFactory.CreateScaleTransformation(5, 5, 5);
            var rot = _transformationsFactory.CreateRotationXTransformation(90);
            _transform = _transform.Combine(rot).Combine(scale);
        }

        public List<PieceModel> LoadPieces(PieceColor color)
        {
            return new List<PieceModel>()
            {
                GetPiece(PieceType.Queen, color),
                GetPiece(PieceType.Knight, color),
                GetPiece(PieceType.Rook, color),
                GetPiece(PieceType.Bishop, color),
                GetPiece(PieceType.Pawn, color),
                GetPiece(PieceType.King, color)
            };

        }

        public PieceModel GetPiece(PieceType type, PieceColor color)
        {
            _objectLoader = new ObjectReader();
            PieceModel piece; 
            switch (type)
            {
                case PieceType.Queen:
                    piece = new PieceModel(type, PieceColor.Black);
                    piece.Triangles = _objectLoader.LoadPiece(type);
                    piece.TransformModel(_transformationsFactory.CreateTranslationTransformation(new Vector3D(-0.05, 0, 0, 0).vector));
                    break;
                case PieceType.Knight:
                    piece = new KnightModel(color);
                    piece.Triangles = _objectLoader.LoadPiece(type);
                    piece.TransformModel(_transformationsFactory.CreateRotationYTransformation(color == PieceColor.White ? -90 : 90));
                    break;
                default:
                    piece = new PieceModel(type, color);
                    piece.Triangles = _objectLoader.LoadPiece(type);
                    break;
            }
            piece.TransformModel(_transform);
            return piece;
        }

        public void SetPieces(List<PieceModel> pieces, double z)
        {
            TransformationsFactory factory = new TransformationsFactory();
            ITransformation translation = factory.CreateTranslationTransformation(Vector<double>.Build.DenseOfArray( new double[] { 1, 0, 0, 0}));
            ITransformation translation2 = factory.CreateIdentityTransformation();
            ITransformation translation3 = factory.CreateTranslationTransformation(Vector<double>.Build.DenseOfArray(new double[] { -2.5, 0, z, 0 }));
            foreach (var piece in pieces)
            {
                piece.TransformModel(translation3);
                piece.TransformModel(translation2);
                translation2 = translation2.Combine(translation);
                piece.ResetModelMatrix();
            }
        }
    }
}
