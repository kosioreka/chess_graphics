﻿using Chess_graphics.Models;
using Chess_graphics.Models.Shading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Managers
{
    public class ShadingManager
    {
        public List<ShadingModel> AvailableShadingModels { get; }
        public ShadingModel ActiveShadingModel { get; private set; }
        private int shadingModelIndex = 0;

        public ShadingManager()
        {
            AvailableShadingModels = new List<ShadingModel>
            {
                new FlatShadingModel(),
                new GouraudShadingModel(),
                new PhongShadingModel(),
                new ShadingModel()
            };
            ActiveShadingModel = AvailableShadingModels[0];
        }

        public void ChangeActiveShadingModel()
        {
             ActiveShadingModel = AvailableShadingModels[++shadingModelIndex % AvailableShadingModels.Count];
        }
    }
}
