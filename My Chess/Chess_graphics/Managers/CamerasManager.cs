﻿using Chess_graphics.Models;
using Chess_graphics.Models.Projection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Managers
{
    public class CamerasManager
    {
        public Camera ActiveCamera { get; private set; }
        public List<Camera> AvailableCameras { get; }

        public CamerasManager(IEnumerable<Camera> cameras)
        {
            var enumerable = cameras as Camera[] ?? cameras.ToArray();
            if (cameras == null || !enumerable.Any())
                throw new ArgumentException();
            AvailableCameras = enumerable.ToList();
            ActiveCamera = AvailableCameras.FirstOrDefault();
        }

        public void ChangeActiveCamera(int index)
        {
            ActiveCamera = AvailableCameras[index % AvailableCameras.Count];
        }

        public void SetMovingPiece(PieceModel piece)
        {
            IEnumerable<ObservableCamera> cam = AvailableCameras.OfType<ObservableCamera>();
            foreach (var observableCamera in cam)
            {
                observableCamera.ObservedModel = piece;
            }
        }
    }
}
