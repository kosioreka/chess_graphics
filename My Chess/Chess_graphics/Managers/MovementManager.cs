﻿using Chess.Models.Transformations;
using Chess_graphics.Models;
using Chess_graphics.Models.BasicModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess_graphics.Managers
{
    public class MovementManager
    {
        public List<PieceModel> AvailableModels { get; }
        public PieceModel SelectedPiece { get; private set; }
        protected ITransformation LeftTransformation { get; set; }
        protected ITransformation RightTransformation { get; set; }
        protected ITransformation ForwardTransformation { get; set; }
        protected ITransformation BackTransformation { get; set; }
        private static readonly double moveStep = 0.1;
        private static readonly int rightAngle = 90;
        private TransformationsFactory factory = new TransformationsFactory();


        public MovementManager()
        {
            LeftTransformation = factory.CreateTranslationTransformation(new Vector3D(-moveStep, 0, 0).vector);
            RightTransformation = factory.CreateTranslationTransformation(new Vector3D(moveStep, 0, 0).vector);
            ForwardTransformation = factory.CreateTranslationTransformation(new Vector3D(0, 0, -moveStep).vector);
            BackTransformation = factory.CreateTranslationTransformation(new Vector3D(0, 0, moveStep).vector);
        }

        public MovementManager(IEnumerable<PieceModel> models, IEnumerable<PieceModel> models2 = null)
        {
            LeftTransformation = factory.CreateTranslationTransformation(new Vector3D(-moveStep, 0, 0).vector);
            RightTransformation = factory.CreateTranslationTransformation(new Vector3D(moveStep, 0, 0).vector);
            ForwardTransformation = factory.CreateTranslationTransformation(new Vector3D(0, 0, -moveStep).vector);
            BackTransformation = factory.CreateTranslationTransformation(new Vector3D(0, 0, moveStep).vector);
            AvailableModels = models.ToList();
            SelectedPiece = AvailableModels.FirstOrDefault();
        }

        public void MovePiece(MovementDirection direction)
        {
            ITransformation transformation = null;
            switch (direction)
            {
                case MovementDirection.Left:
                    transformation = LeftTransformation;
                    break;
                case MovementDirection.Right:
                    transformation = RightTransformation;
                    break;
                case MovementDirection.Up:
                    transformation = ForwardTransformation;
                    break;
                case MovementDirection.Down:
                    transformation = BackTransformation;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
            SelectedPiece.TransformModel(transformation);
            if(SelectedPiece.Type == PieceType.Knight)
            {
                RotateKnight(direction);
            }
        }

        private void RotateKnight(MovementDirection direction)
        {
            int angle = - (direction - ((KnightModel)SelectedPiece).Direction) * rightAngle;
            var vector = SelectedPiece.ModelMatrix * SelectedPiece.Triangles[0].Vertices[0].Point.vector;
            SelectedPiece.TransformModel(factory.CreateTranslationTransformation(-vector));
            SelectedPiece.TransformModel(factory.CreateRotationYTransformation(angle));
            SelectedPiece.TransformModel(factory.CreateTranslationTransformation(vector));
            ((KnightModel)SelectedPiece).Direction = direction;
        }

        public void ChangeSelectedPiece(PieceType type)
        {
            SelectedPiece = AvailableModels.FirstOrDefault(p => p.Type == type);
        }
    }

    public enum MovementDirection
    {
        Up,
        Right,
        Down,
        Left
    }
}
