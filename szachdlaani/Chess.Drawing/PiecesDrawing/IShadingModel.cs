﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;
using Chess.Models.Models;
using Chess.Models.Projection;

namespace Chess.Drawing.PiecesDrawing
{
    public interface IShadingModel
    {
        void DrawModels(WriteableBitmap bitmap, IEnumerable<ITriangleModel> models, Camera activeCamera, IList<Light> lights);
    }
}