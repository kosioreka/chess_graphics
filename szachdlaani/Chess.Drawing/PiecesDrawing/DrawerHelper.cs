﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Drawing.Illumination;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Drawing.PiecesDrawing
{
    public static class DrawerHelper
    {
        public static Triangle TransformTriangle(Triangle triangle, int width, int height, Matrix<double> transformMatrix, Matrix<double> modelMatrix = null)
        {
            var invTr = transformMatrix.Inverse().Transpose();
            var invMo = modelMatrix?.Inverse()?.Transpose();
            IList<Vertex> v = new List<Vertex>();
            foreach (var vertex in triangle.Vertices)
            {
                Vector3D p = vertex.Point.Clone();
                Vector3D n = vertex.Normal.Clone();
                p = transformMatrix * p;
                p.Normalize2();
                p.X = width + width * p.X;
                p.Y = height - height * p.Y;
                n = invMo==null?invTr*n: invMo*n;
                n.Normalize();
                v.Add(new Vertex(p, n));
            }
            return new Triangle(v[0], v[1], v[2]);
        }

        public static double Clamp(double value, double min = 0, double max = 1)
        {
            return Math.Max(min, Math.Min(value, max));
        }

        public static double Interpolate(double min, double max, double gradient)
        {
            return min + (max - min) * Clamp(gradient);
        }

        public static Intensity Interpolate(Intensity info1, Intensity info2, double gradient)
        {
            double r = Interpolate(info1.R, info2.R, gradient);
            double g = Interpolate(info1.G, info2.G, gradient);
            double b = Interpolate(info1.B, info2.B, gradient);
            return new Intensity(r,g,b);
        }

        public static Vector3D Interpolate(Vector3D vector1, Vector3D vector2, double gradient)
        {
            double x = Interpolate(vector1.X, vector2.X, gradient);
            double y = Interpolate(vector1.Y, vector2.Y, gradient);
            double z = Interpolate(vector1.Z, vector2.Z, gradient);
            Vector3D normal = new Vector3D(x,y,z);
            normal.Normalize();
            return normal;
        }

        public static Point3D Interpolate(Point3D point1, Point3D point2, double gradient)
        {
            double x = Interpolate(point1.X, point2.X, gradient);
            double y = Interpolate(point1.Y, point2.Y, gradient);
            double z = Interpolate(point1.Z, point2.Z, gradient);
            return new Point3D(x,y,z);
        }

        public static void SortVerticesByY(ref Vertex p1, ref Vertex p2, ref Vertex p3)
        {
            if (p1.Point.Y > p2.Point.Y)
            {
                var temp = p2;
                p2 = p1;
                p1 = temp;
            }

            if (p2.Point.Y > p3.Point.Y)
            {
                var temp = p2;
                p2 = p3;
                p3 = temp;
            }
            if (p1.Point.Y > p2.Point.Y)
            {
                var temp = p2;
                p2 = p1;
                p1 = temp;
            }
        }

        internal static void SortVerticesByY(ref IntensityVertex p1, ref IntensityVertex p2, ref IntensityVertex p3)
        {
            if (p1.Coordinates.Point.Y > p2.Coordinates.Point.Y)
            {
                var temp = p2;
                p2 = p1;
                p1 = temp;
            }

            if (p2.Coordinates.Point.Y > p3.Coordinates.Point.Y)
            {
                var temp = p2;
                p2 = p3;
                p3 = temp;
            }
            if (p1.Coordinates.Point.Y > p2.Coordinates.Point.Y)
            {
                var temp = p2;
                p2 = p1;
                p1 = temp;
            }
        }

        internal static void CalculateCoefficients(double p1X, double p1Y, double p2X, double p2Y, double p3X, double p3Y, out double dP1P2, out double dP1P3)
        {
            if (p2Y - p1Y > 0)
                dP1P2 = (p2X - p1X) / (p2Y - p1Y);
            else
                dP1P2 = 0;
            if (p3Y - p1Y > 0)
                dP1P3 = (p3X - p1X) / (p3Y - p1Y);
            else
                dP1P3 = 0;
        }
    }
}
