using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Chess.Drawing.Device;
using Chess.Models.Models;
using Chess.Models.Models.Primitives;
using Chess.Models.Projection;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Drawing.PiecesDrawing
{
    public abstract class ShadingModel : IShadingModel
    {
        protected Matrix<double> ViewMatrix;
        protected Matrix<double> ProjectionMatrix;
        protected int W;
        protected int H;
        protected Camera Camera;
        protected IList<Light> Lights;

        private WriteableBitmapDevice _device; 
        private int _bitmapWidth;
        private int _bitmapHeight;
        private double[,] _zBuffer;

        public void DrawModels(WriteableBitmap bitmap, IEnumerable<ITriangleModel> models, Camera activeCamera,
            IList<Light> lights)
        {
            _device = new WriteableBitmapDevice(bitmap);
            _device.BeginDrawing();
            _device.Clear(Colors.LightBlue);
            _zBuffer = new double[bitmap.PixelWidth, bitmap.PixelHeight];
            for (int i = 0; i < _zBuffer.GetLength(0); i++)
                for (int j = 0; j < _zBuffer.GetLength(1); j++)
                    _zBuffer[i, j] = double.MaxValue;
            _bitmapWidth = bitmap.PixelWidth;
            _bitmapHeight = bitmap.PixelHeight;
            Camera = activeCamera;
            Lights = lights;
            W = bitmap.PixelWidth / 2;
            H = bitmap.PixelHeight / 2;
            ViewMatrix = Camera.CreateLookAt();
            ProjectionMatrix = Camera.CreateProjectionView(1, 100, (double)H/W);
            foreach (var triangleModel in models)
            {
                FillModel(triangleModel);
            }
            _device.EndDrawing();
        }

        protected virtual void FillModel(ITriangleModel triangleModel)
        {
            Matrix<double> transformMatrix = (ProjectionMatrix * (ViewMatrix * triangleModel.ModelMatrix));
            foreach (var triangle in triangleModel.Triangles)
            {
                Triangle triangle1 = DrawerHelper.TransformTriangle(triangle, W, H, transformMatrix);
                FillTriangle(triangle1.Vertices[0], triangle1.Vertices[1], triangle1.Vertices[2], triangleModel.BaseColor);
            }
        }

        protected void FillTriangle(Vertex p1, Vertex p2, Vertex p3, Color color)
        {
            DrawerHelper.SortVerticesByY(ref p1,ref p2, ref p3);
            double dP1P2, dP1P3;
            DrawerHelper.CalculateCoefficients(p1.Point.X, p1.Point.Y, p2.Point.X, p2.Point.Y, p3.Point.X, p3.Point.Y, out dP1P2, out dP1P3);
            if (dP1P2 > dP1P3)
            {
                for (var y = (int)p1.Point.Y; y <= (int)p3.Point.Y; y++)
                {
                    if (y < p2.Point.Y)
                    {
                        ProcessScanLine(y, p1, p3, p1, p2, color);
                    }
                    else
                    {
                        ProcessScanLine(y, p1, p3, p2, p3, color);
                    }
                }
            }
            else
            {
                for (var y = (int)p1.Point.Y; y <= (int)p3.Point.Y; y++)
                {
                    if (y < p2.Point.Y)
                    {
                        ProcessScanLine(y, p1, p2, p1, p3, color);
                    }
                    else
                    {
                        ProcessScanLine(y, p2, p3, p1, p3, color);
                    }
                }
            }
        }

        protected void ProcessScanLine(int y, Vertex pa, Vertex pb, Vertex pc, Vertex pd, Color color)
        {
            var gradient1 = (int)pa.Point.Y != (int)pb.Point.Y ? (y - pa.Point.Y) / (pb.Point.Y - pa.Point.Y) : 1;
            var gradient2 = (int)pc.Point.Y != (int)pd.Point.Y ? (y - pc.Point.Y) / (pd.Point.Y - pc.Point.Y) : 1;
            int sx = (int)DrawerHelper.Interpolate(pa.Point.X, pb.Point.X, gradient1);
            int ex = (int)DrawerHelper.Interpolate(pc.Point.X, pd.Point.X, gradient2);
            double z1 = DrawerHelper.Interpolate(pa.Point.Z, pb.Point.Z, gradient1);
            double z2 = DrawerHelper.Interpolate(pc.Point.Z, pd.Point.Z, gradient2);
            for (var x = sx; x < ex; x++)
            {
                double gradient = (x - sx) / (double)(ex - sx);
                var z = DrawerHelper.Interpolate(z1, z2, gradient);
                DrawPoint(new Point3D(x, y, z), color);
            }
        }

        protected void DrawPoint(Point3D point, Color color)
        {
            if (point.X >= 0 && point.Y >= 0 && point.X < _bitmapWidth && point.Y < _bitmapHeight)
            {
                int x = (int) point.X;
                int y = (int) point.Y;
                if (_zBuffer[x, y] < point.Z) return;
                _zBuffer[x, y] = point.Z;
                _device.SetPixel(x, y, color);
            }
        }
    }
}