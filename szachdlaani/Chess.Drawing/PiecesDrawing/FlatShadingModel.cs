﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Chess.Drawing.Illumination;
using Chess.Models.Models;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Drawing.PiecesDrawing
{
    public class FlatShadingModel : ShadingModel
    {
        protected override void FillModel(ITriangleModel triangleModel)
        {
            Matrix<double> transformMatrix = (ProjectionMatrix * (ViewMatrix * triangleModel.ModelMatrix));
            foreach (var triangle in triangleModel.Triangles)
            {
                Triangle triangle1 = DrawerHelper.TransformTriangle(triangle, W, H, transformMatrix, triangleModel.ModelMatrix);
                Vertex position = CalculateMiddlePoint(triangle);       
                var m = triangleModel.ModelMatrix.Inverse().Transpose();
                position.Point = triangleModel.ModelMatrix * (Vector3D)position.Point;
                position.Normal = m * position.Normal;
                position.Normal.Normalize();
                var intensity = IlluminationHelper.CalculateIntensity(position, triangleModel.IlluminationProperties, Lights, Camera);
                Color color = triangleModel.BaseColor;
                color.R = (byte) (Math.Min(255, intensity.R*triangleModel.BaseColor.R));
                color.G = (byte)(Math.Min(255, intensity.G * triangleModel.BaseColor.G));
                color.B = (byte)(Math.Min(255, intensity.B * triangleModel.BaseColor.B));
                FillTriangle(triangle1.Vertices[0], triangle1.Vertices[1], triangle1.Vertices[2], color);
            }
        }

        private Vertex CalculateMiddlePoint(Triangle triangle)
        {
            var vector = (triangle.Vertices[0].Normal + triangle.Vertices[1].Normal + triangle.Vertices[2].Normal) / 3;
            vector.Normalize();
            Point3D position = ((Vector3D)triangle.Vertices[0].Point + triangle.Vertices[1].Point + triangle.Vertices[2].Point) / 3;
            return new Vertex(position, vector);
        }
    }
}
