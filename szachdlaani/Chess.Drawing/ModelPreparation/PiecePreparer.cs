﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;
using Chess.Models.Pieces;
using Chess.Models.Transformations;

namespace Chess.Drawing.ModelPreparation
{
    public class PiecePreparer : IPiecePreparer
    {
        private readonly PiecesFactory _piecesFactory = new PiecesFactory();
        private readonly TransformationsFactory _transformationsFactory = new TransformationsFactory();
        private readonly ITransformation _transform;

        public PiecePreparer()
        {
            _transform = _transformationsFactory.CreateRotationXTransformation(-90);
            var scale = _transformationsFactory.CreateScaleTransformation(5, 5, 5);
            var rot = _transformationsFactory.CreateRotationXTransformation(90);
            _transform = _transform.Combine(rot).Combine(scale);
        }
         
        public Piece PreparePiece(PieceType type, PieceColor color)
        {
            switch (type)
            {
                case PieceType.Pawn:
                    return PreparePawn(color);
                case PieceType.Knight:
                    return PrepareKnight(color);
                case PieceType.Bishop:
                    return PrepareBishop(color);
                case PieceType.Rook:
                    return PrepareRook(color);
                case PieceType.Queen:
                    return PrepareQueen(color);
                case PieceType.King:
                    return PrepareKing(color);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        protected virtual Piece PrepareKing(PieceColor color)
        {
            Piece piece = _piecesFactory.Create(PieceType.King, color);
            piece.TransformModel(_transform);
            return piece;
        }

        protected virtual Piece PrepareQueen(PieceColor color)
        {
            Piece piece = _piecesFactory.Create(PieceType.Queen, color);
            piece.TransformModel(_transformationsFactory.CreateTranslationTransformation(new Vector3D(-0.05,0,0,0)));
            piece.TransformModel(_transform);
            return piece;
        }

        protected virtual Piece PrepareRook(PieceColor color)
        {
            Piece piece = _piecesFactory.Create(PieceType.Rook, color);
            piece.TransformModel(_transform);
            return piece;
        }

        protected virtual Piece PrepareBishop(PieceColor color)
        {
            Piece piece = _piecesFactory.Create(PieceType.Bishop, color);
            piece.TransformModel(_transform);
            return piece;
        }

        protected virtual Piece PrepareKnight(PieceColor color)
        {
            Piece piece = _piecesFactory.Create(PieceType.Knight, color);
            piece.TransformModel(_transformationsFactory.CreateRotationYTransformation(-90));
            piece.TransformModel(_transform);
            return piece;
        }

        protected virtual Piece PreparePawn(PieceColor color)
        {
            Piece piece = _piecesFactory.Create(PieceType.Pawn, color);
            piece.TransformModel(_transform);
            return piece;
        }
    }
}
