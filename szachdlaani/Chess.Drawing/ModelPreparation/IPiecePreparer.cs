﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Pieces;

namespace Chess.Drawing.ModelPreparation
{
    public interface IPiecePreparer
    {
        Piece PreparePiece(PieceType type, PieceColor color);
    }
}
