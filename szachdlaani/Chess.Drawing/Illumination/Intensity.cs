﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Chess.Drawing.Illumination
{
    public class Intensity
    {
        public double R;
        public double G;
        public double B;

        public Intensity(double r, double g, double b)
        {
            R = r;
            G = g;
            B = b;
        }

        public static Intensity operator +(Intensity info1, Intensity info2)
        {
            return new Intensity(info1.R+info2.R, info1.G+info2.G, info1.B+info2.B);
        }

    }
}
