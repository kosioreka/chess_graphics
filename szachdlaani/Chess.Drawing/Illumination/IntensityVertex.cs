﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;

namespace Chess.Drawing.Illumination
{
    public class IntensityVertex
    {
        public Vertex Coordinates { get; set; }
        public Vertex WorldCoordinates { get; set; }
        public Intensity Intensity { get; set; }

        public IntensityVertex(Vertex coordinates, Vertex worldCoordinates, Intensity intensity)
        {
            Coordinates = coordinates;
            WorldCoordinates = worldCoordinates;
            Intensity = intensity;
        }
    }
}
