﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Chess.Models.Models;
using Chess.Models.Models.Primitives;
using Chess.Models.Projection;

namespace Chess.Drawing.Illumination
{
    public static class IlluminationHelper
    {
        public static Intensity CalculateIntensity(Vertex position, IlluminationProperties properties, Light light, Camera observer)
        {
            //ambient
            double ra = light.AmbientIntensity*properties.AmbientReflections.R;
            double ga = light.AmbientIntensity*properties.AmbientReflections.G;
            double ba = light.AmbientIntensity*properties.AmbientReflections.B;
            //vectors
            Vector3D toLight = light.Position - (Vector3D)position.Point;
            double distance = toLight.Length();
            double d = distance * distance;
            toLight.Normalize();
            Vector3D normal = position.Normal.Clone();
            normal.Normalize();
            Vector3D toObserver = observer.CameraPosition - (Vector3D)position.Point;
            toObserver.Normalize();
            Vector3D reflection = toLight.Symmetry(normal);
            reflection.Normalize();
            //diffuse
            double cosDiffuse = toLight.AngleCos(normal);
            double rd = properties.DiffuseReflections.R * light.DiffuseIntensity * Math.Max(cosDiffuse, 0);
            double gd = properties.DiffuseReflections.G * light.DiffuseIntensity * Math.Max(cosDiffuse, 0);
            double bd = properties.DiffuseReflections.B * light.DiffuseIntensity * Math.Max(cosDiffuse, 0);
            //specular
            double cosSpecular = reflection.AngleCos(toObserver);
            double cosn = Math.Pow(Math.Max(cosSpecular, 0), properties.NShine);
            double rs = properties.SpecularReflections.R * light.SpecularIntensity * cosn;
            double gs = properties.SpecularReflections.G * light.SpecularIntensity * cosn;
            double bs = properties.SpecularReflections.B * light.SpecularIntensity * cosn;
            //total intensity
            return new Intensity(ra+rd+rs, ga+gd+gs, ba+bd+bs);
        }

        public static Intensity CalculateIntensity(Vertex position, IlluminationProperties properties, IEnumerable<Light> lights, Camera observer)
        {
            Intensity intensity = new Intensity(0, 0, 0);
            return lights.Aggregate(intensity, (current, light) => current + CalculateIntensity(position, properties, light, observer));
        }
    }
}
