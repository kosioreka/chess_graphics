﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Chess.Drawing.Device
{
    public class WriteableBitmapDevice
    {
        private readonly WriteableBitmap _bitmap;
        private readonly int _renderWidth;
        private readonly int _renderHeight;
        private byte[] _backBuffer;
  
        public WriteableBitmapDevice(WriteableBitmap bitmap)
        {
            _bitmap = bitmap;
            _renderWidth = _bitmap.PixelWidth;
            _renderHeight = _bitmap.PixelHeight;
        }

        public void BeginDrawing()
        {
            _backBuffer = new byte[_renderWidth*_renderHeight*4];
            _bitmap.Lock();
        }

        public void EndDrawing()
        {
            _bitmap.WritePixels(new Int32Rect(0,0,_renderWidth, _renderHeight),_backBuffer, 4*_renderWidth,0);
            _bitmap.Unlock();
        }

        public void SetPixel(int x, int y, Color color)
        {
            var index = (x + y * _renderWidth);
            var index4 = index * 4;
            _backBuffer[index4] = color.B;
            _backBuffer[index4 + 1] = color.G;
            _backBuffer[index4 + 2] = color.R;
            _backBuffer[index4 + 3] = color.A;
        }

        public void Clear(byte b, byte g, byte r, byte a)
        {
            for (var index = 0; index < _backBuffer.Length; index += 4)
            {
                _backBuffer[index] = b;
                _backBuffer[index + 1] = g;
                _backBuffer[index + 2] = r;
                _backBuffer[index + 3] = a;
            }
        }

        public void Clear(Color color)
        {
            Clear(color.B,color.G,color.R, color.A);
        }
    }
}
