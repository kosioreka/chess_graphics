﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;
using Chess.Models.Pieces;
using ObjLoader.Loader.Loaders;

namespace Chess.Models.Reader
{
    internal class PieceReader : ObjectLoader
    {
        public PieceReader() { }

        public IList<Triangle> LoadPiece(PieceType type)
        {
            switch (type)
            {
                case PieceType.Pawn:
                    return LoadPiece("Pawn");
                case PieceType.Knight:
                    return LoadPiece("Knight");
                case PieceType.Bishop:
                    return LoadPiece("Bishop");
                case PieceType.Rook:
                    return LoadPiece("Rook");
                case PieceType.Queen:
                    return LoadPiece("Queen");
                case PieceType.King:
                    return LoadPiece("King");
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private IList<Triangle> LoadPiece(string name)
        {
            IList<Triangle> triangles = new List<Triangle>();
            LoadResult result = Load("./PiecesModels/" + name + ".obj");
            foreach (var group in result.Groups)
            {
                foreach (var face in group.Faces)
                {
                    List<Vertex> vertices = new List<Vertex>();
                    for (int i = 0; i < face.Count; i++)
                    {
                        var vertex = result.Vertices[face[i].VertexIndex-1];
                        var normal = result.Normals[face[i].NormalIndex-1];
                        Point3D point = new Point3D(vertex.X,vertex.Y,vertex.Z);
                        Vector3D vector = new Vector3D(normal.X, normal.Y, normal.Z);
                        vector.Normalize();
                        Vertex v = new Vertex(point, vector);
                        vertices.Add(v);
                    }
                    if(vertices.Count!=3) throw new InvalidOperationException();
                    Triangle triangle = new Triangle(vertices[0], vertices[1], vertices[2]);
                    triangles.Add(triangle);
                }   
            }
            return triangles;
        }
    }
}
