﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjLoader.Loader.Loaders;

namespace Chess.Models.Reader
{
    public class ObjectLoader
    {
        private readonly IObjLoader _loader;

        public ObjectLoader()
        {
            var factory = new ObjLoaderFactory();
            _loader = factory.Create();
        }

        public LoadResult Load(string path)
        {
            try
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    return _loader.Load(stream);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
