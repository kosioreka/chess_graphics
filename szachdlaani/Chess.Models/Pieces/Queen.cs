﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public sealed class Queen : Piece
    {
        internal Queen(PieceColor color) : base(PieceType.Queen, color) { }

        internal Queen(PieceColor color, Matrix<double> modelMatrix) : base(PieceType.Queen, color, modelMatrix) { }

    }
}
