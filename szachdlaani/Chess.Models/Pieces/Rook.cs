﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public sealed class Rook : Piece
    {
        internal Rook(PieceColor color) : base(PieceType.Rook, color) { }

        internal Rook(PieceColor color, Matrix<double> modelMatrix) : base(PieceType.Rook, color, modelMatrix) { }

    }
}
