﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public sealed class Knight : Piece
    {
        internal Knight(PieceColor color) : base(PieceType.Knight, color) { }

        internal Knight(PieceColor color, Matrix<double> modelMatrix) : base(PieceType.Knight, color, modelMatrix) { }

    }
}
