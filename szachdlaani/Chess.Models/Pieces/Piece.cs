﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Chess.Models.Models;
using Chess.Models.Models.Primitives;
using Chess.Models.Reader;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public abstract class Piece : TriangleModel, IPiece
    {
        public PieceType Type { get; private set; }
        public PieceColor Color { get; }

        protected Piece(PieceType type, PieceColor color) : this(type,color, Matrix<double>.Build.DenseIdentity(4)) { }

        protected Piece(PieceType type, PieceColor color, Matrix<double> modelMatrix) : base(modelMatrix)
        {
            Type = type;
            Color = color;
            BaseColor = Color == PieceColor.White ? Colors.White : System.Windows.Media.Color.FromRgb(40,40,40);
        }

        internal void LoadPiece()
        {
            PieceReader reader = new PieceReader();
            Triangles = reader.LoadPiece(Type);
        }
    }
}
