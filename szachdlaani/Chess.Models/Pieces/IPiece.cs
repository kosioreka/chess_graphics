﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models;

namespace Chess.Models.Pieces
{
    public interface IPiece : ITriangleModel
    {
        PieceType Type { get; }
        PieceColor Color { get; }
    }
}
