﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public class PiecesFactory
    {
        public Piece Create(PieceType type, PieceColor color)
        {
            return Create(type, color, Matrix<double>.Build.DenseIdentity(4));
        }

        public Piece Create(PieceType type, PieceColor color, Matrix<double> modelMatrix)
        {
            Piece piece=null;
            switch (type)
            {
                case PieceType.Pawn:
                    piece = new Pawn(color, modelMatrix);
                    break;
                case PieceType.Knight:
                    piece = new Knight(color,modelMatrix);
                    break;
                case PieceType.Bishop:
                    piece = new Bishop(color, modelMatrix);
                    break;
                case PieceType.Rook:
                    piece = new Rook(color,modelMatrix);
                    break;
                case PieceType.Queen:
                    piece = new Queen(color, modelMatrix);
                    break;
                case PieceType.King:
                    piece = new King(color, modelMatrix);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
            piece.LoadPiece();
            return piece;
        }
    }
}
