﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public sealed class Bishop : Piece
    {
        internal Bishop(PieceColor color) : base(PieceType.Bishop, color) { }

        internal Bishop(PieceColor color, Matrix<double> modelMatrix) : base(PieceType.Bishop, color, modelMatrix) { }

    }
}
