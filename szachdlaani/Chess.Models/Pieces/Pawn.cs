﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public sealed class Pawn : Piece
    {
        internal Pawn(PieceColor color) : this(color, Matrix<double>.Build.DenseIdentity(4)) { }

        internal Pawn(PieceColor color, Matrix<double> modelMatrix) : base(PieceType.Pawn, color, modelMatrix) { }

    }
}
