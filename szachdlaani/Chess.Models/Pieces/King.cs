﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Pieces
{
    public sealed class King : Piece
    {
        internal King(PieceColor color) : base(PieceType.King, color) { }

        internal King(PieceColor color, Matrix<double> modelMatrix) : base(PieceType.King, color, modelMatrix) { }

    }
}
