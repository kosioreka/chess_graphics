﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class RotationYTransformation : RotationTransformation
    {
        public RotationYTransformation(double angleInDegrees) : base()
        {
            Line = new Vector3D(0, 1, 0);
            AngleInDegrees = angleInDegrees;
            double angle = angleInDegrees * Math.PI / 180;
            double c = Math.Cos(angle);
            double s = Math.Sin(angle);
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {c, 0, s, 0},
                {0, 1, 0, 0},
                {-s,0, c, 0},
                {0, 0, 0, 1}
            });
        }
    }
}
