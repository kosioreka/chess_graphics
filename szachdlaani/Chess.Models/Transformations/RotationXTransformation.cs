﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class RotationXTransformation : RotationTransformation
    {
        public RotationXTransformation(double angleInDegrees)
        {
            Line = new Vector3D(1,0,0);
            AngleInDegrees = angleInDegrees;
            double angle = angleInDegrees*Math.PI/180;
            double c = Math.Cos(angle);
            double s = Math.Sin(angle);
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {1, 0, 0, 0},
                {0, c, -s, 0},
                {0, s, c, 0},
                {0, 0, 0, 1}
            });
        }
    }
}
