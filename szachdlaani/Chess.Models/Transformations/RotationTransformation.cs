﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class RotationTransformation : MatrixTransformation
    {
        public RotationTransformation(Vector3D line, double angleInDegrees)
        {
            if(line==null) throw new ArgumentNullException(nameof(line));
            Line = line;
            AngleInDegrees = angleInDegrees;
            double angle = angleInDegrees*Math.PI/180;
            double s = Math.Sin(angle);
            double c = Math.Cos(angle);
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                { line.X*line.X+(1-line.X)*(1-line.X)*c, line.X*line.Y*(1-c)-line.Z*s, line.X*line.Z*(1-c)+line.Y*s, 0},
                { line.Y*line.X*(1-c)+line.Z*s, line.Y*line.Y+(1-line.Y)*(1-line.Y)*c, line.Y*line.Z*(1-c)-line.X*s,0},
                { line.X*line.Z*(1-c)-line.Y*s, line.Y*line.Z*(1-c)+line.X*s, line.Z*line.Z + (1-line.Z)*(1-line.Z)*c,0},
                {0,0,0,1 }
            });
        }

        protected RotationTransformation() { }

        public Vector3D Line { get; protected set; }
        public double AngleInDegrees { get; protected set; }
    }
}
