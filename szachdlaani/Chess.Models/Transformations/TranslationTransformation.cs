﻿using System;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class TranslationTransformation : MatrixTransformation
    {
        public TranslationTransformation(Vector3D vector) : base()
        {
            if(vector==null) throw new ArgumentNullException(nameof(vector));
            Vector = vector;
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {1, 0, 0, vector.X},
                {0, 1, 0, vector.Y},
                {0, 0, 1, vector.Z},
                {0, 0, 0, 1}
            });
        }

        public Vector3D Vector { get; }
    }
}