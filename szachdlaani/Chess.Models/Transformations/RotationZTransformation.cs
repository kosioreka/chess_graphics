﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    internal class RotationZTransformation : RotationTransformation
    {
        public RotationZTransformation(double angleInDegrees) : base()
        {
            Line = new Vector3D(0, 0, 1,0);
            AngleInDegrees = angleInDegrees;
            double angle = angleInDegrees * Math.PI / 180;
            double c = Math.Cos(angle);
            double s = Math.Sin(angle);
            Matrix = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {c, -s, 0, 0},
                {s, c, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
            });
        }
    }
}
