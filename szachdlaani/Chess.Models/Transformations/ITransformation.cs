﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Transformations
{
    public interface ITransformation
    {
        Point3D Transform(Point3D point);
        Matrix<double> Matrix { get; }
    }

    public static class TransformationExtensions
    {
        public static Vector3D TransformVector(this ITransformation transformation, Vector3D vector)
        {
            return transformation.Transform(vector);
        }

        public static void TransformModel(this ITriangleModel model, ITransformation transformation)
        {
            var matrix = transformation.Matrix * model.ModelMatrix;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    model.ModelMatrix[i, j] = matrix[i, j];
                }
            }
        }

        public static ITransformation Combine(this ITransformation transformation, ITransformation transformation2)
        {
            return
                new TransformationsFactory().CreateCompositionTransformation(new ITransformation[]
                {transformation, transformation2});
        }
    }
}
