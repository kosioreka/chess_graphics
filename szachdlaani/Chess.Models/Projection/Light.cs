﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Chess.Models.Models.Primitives;

namespace Chess.Models.Projection
{
    public abstract class Light
    {
        protected Light(Point3D position, double ambient, double diffuse, double specular)
        {
            Position = position;
            AmbientIntensity = ambient;
            DiffuseIntensity = diffuse;
            SpecularIntensity = specular;
        }

        public Point3D Position { get; }
        public double AmbientIntensity { get; }
        public double DiffuseIntensity { get; }
        public double SpecularIntensity { get; }
    }
}
