﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Projection
{
    public abstract class Camera
    {
        protected Camera(Point3D cameraPosition, Point3D cameraTarget, Vector3D upVector, double fieldOfView = 90)
        {
            CameraPosition = cameraPosition;
            CameraTarget = cameraTarget;
            UpVector = upVector;
            FieldOfView = fieldOfView;
        }

        public virtual Point3D CameraPosition { get; set; }
        public virtual Point3D CameraTarget { get; set; }
        public Vector3D UpVector { get; private set; }
        public double FieldOfView { get; private set; }

        public virtual Matrix<double> CreateLookAt()
        {
            Vector3D zAxis = (Vector3D)CameraPosition - (Vector3D)CameraTarget;
            zAxis.Normalize();
            Vector3D xAxis = UpVector.CrossProduct(zAxis);
            xAxis.Normalize();
            Vector3D yAxis = zAxis.CrossProduct(xAxis);
            yAxis.Normalize();
            Matrix<double> viewInversed = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {xAxis.X, yAxis.X, zAxis.X, CameraPosition.X},
                {xAxis.Y, yAxis.Y, zAxis.Y, CameraPosition.Y },
                {xAxis.Z, yAxis.Z, zAxis.Z, CameraPosition.Z },
                {0,0,0,1 }
            });
            return viewInversed.Inverse();
        }

        public virtual Matrix<double> CreateProjectionView(double near, double far, double a)
        {
            double e = 1 / Math.Tan(FieldOfView * Math.PI / 360);
            return Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {e,0,0,0 },
                { 0,e/a,0,0 },
                {0,0,-(far+near)/(far-near), -2*far*near/(far-near) },
                {0,0,-1,0 }
            });
        }
    }
}
