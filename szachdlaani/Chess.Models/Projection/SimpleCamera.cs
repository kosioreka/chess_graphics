﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models.Primitives;

namespace Chess.Models.Projection
{
    public class SimpleCamera : Camera
    {
        public SimpleCamera(Point3D cameraPosition, Point3D cameraTarget, Vector3D upVector, double fieldOfView = 90) 
            : base(cameraPosition, cameraTarget, upVector, fieldOfView) { }
    }
}
