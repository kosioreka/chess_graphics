﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Chess.Models.Models.Primitives;

namespace Chess.Models.Projection
{
    public class PointLight : Light
    {
        public PointLight(Point3D position) :base(position, 0.4,1,1) { }
    }
}
