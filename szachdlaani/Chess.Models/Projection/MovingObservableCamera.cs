﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models;
using Chess.Models.Models.Primitives;

namespace Chess.Models.Projection
{
    public class MovingObservableCamera : ObservableCamera
    {
        public MovingObservableCamera(ITriangleModel model, Vector3D observeFrom, Vector3D upVector, double fieldOfView = 90) 
            : base(new Point3D(0,0,0,0), upVector, model, fieldOfView)
        {
            ObserveFrom = observeFrom;
        }

        public Vector3D ObserveFrom { get; set; }

        public override Point3D CameraPosition => (CameraTarget - ObserveFrom);
    }
}
