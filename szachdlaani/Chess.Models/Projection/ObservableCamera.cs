﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chess.Models.Models;
using Chess.Models.Models.Primitives;

namespace Chess.Models.Projection
{
    public class ObservableCamera : Camera
    {
        public ObservableCamera(Point3D cameraPosition, Vector3D upVector, ITriangleModel observedModel, double fieldOfView = 90)
            : base(cameraPosition, new Point3D(0,0,0), upVector, fieldOfView)
        {
            ObservedModel = observedModel;
        }

        public ITriangleModel ObservedModel { get; set; }

        public override Point3D CameraTarget => ObservedModel.ModelMatrix*(Vector3D)ObservedModel.Triangles[0].Vertices[0].Point;
    }
}
