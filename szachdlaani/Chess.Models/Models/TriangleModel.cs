﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Chess.Models.Models.Primitives;
using Chess.Models.Transformations;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Models
{
    public class TriangleModel : ITriangleModel
    {
        public IList<Triangle> Triangles { get; protected set; }
        public Matrix<double> ModelMatrix { get; protected set; }

        public TriangleModel(IList<Triangle> triangles, Matrix<double> modelMatrix, IlluminationProperties properties=null)
        {
            if(triangles==null) throw new ArgumentNullException(nameof(triangles));
            if(modelMatrix==null) throw new ArgumentNullException(nameof(modelMatrix));
            if(modelMatrix.RowCount!=4 || modelMatrix.ColumnCount!=4)
                throw new ArgumentException("Invalid matrix dimentions", nameof(modelMatrix));
            Triangles = triangles;
            ModelMatrix = modelMatrix;
            IlluminationProperties = properties ?? new IlluminationProperties()
            {
                AmbientReflections = new IntensityCoefficients() { R=0.5, G=0.5, B=0.5},
                DiffuseReflections = new IntensityCoefficients() { R=0.95, G=0.95, B=0.95},
                SpecularReflections = new IntensityCoefficients() { R=0.6,G=0.6, B=0.6},
                NShine = 10
            };
        }

        public TriangleModel(IList<Triangle> triangles) : this(triangles,Matrix<double>.Build.DenseIdentity(4)) { }

        protected TriangleModel() : this(new List<Triangle>()) { }

        protected TriangleModel(Matrix<double> modelMatrix) : this(new List<Triangle>(), modelMatrix) { }

        public Color BaseColor { get; set; }

        public IlluminationProperties IlluminationProperties { get; }

        public void ResetModelMatrix()
        {
            var matrix = ModelMatrix;
            var inv = matrix.Inverse().Transpose();
            foreach (var triangle in Triangles)
            {
                foreach (var vertex in triangle.Vertices)
                {
                    vertex.Point = matrix * (Vector3D)vertex.Point;
                    vertex.Normal = inv * vertex.Normal;
                    vertex.Normal.Normalize();
                    vertex.Normal.W = 0;
                }
            }
            ModelMatrix = Matrix<double>.Build.DenseIdentity(4);
        }
    }
}
