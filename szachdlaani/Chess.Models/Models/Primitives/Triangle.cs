﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models.Models.Primitives
{
    public class Triangle
    {
        public Vertex[] Vertices { get; }

        public Triangle(Vertex a, Vertex b, Vertex c)
        {
            if(a==null) throw new ArgumentNullException(nameof(a));
            if(b==null) throw new ArgumentNullException(nameof(b));
            if(c==null) throw new ArgumentNullException(nameof(c));
            Vertices = new[] {a, b, c};
        }
    }
}
