﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models.Models.Primitives
{
    public class Segment
    {
        public Segment(Point3D start, Point3D end)
        {
            if (start == null) throw new ArgumentNullException(nameof(start));
            if (end == null) throw new ArgumentNullException(nameof(end));
            Start = start;
            End = end;
        }

        public Point3D Start { get; set; }
        public Point3D End { get; set; }
    }
}
