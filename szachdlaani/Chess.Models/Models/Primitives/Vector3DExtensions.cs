﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models.Models.Primitives
{
    public static class Vector3DExtensions
    {
        public static void Normalize(this Vector3D vector)
        {
            double length = vector.Length();
            if (Math.Abs(length) <= 0) return;
            vector.X = vector.X / length;
            vector.Y = vector.Y / length;
            vector.Z = vector.Z / length;
            vector.W = vector.W / length;
        }

        public static double Length(this Vector3D vector)
        {
            return Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z);
        }

        public static double Length2(this Vector3D vector)
        {
            return Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z + vector.W * vector.W);
        }

        public static void Normalize2(this Vector3D vector)
        {
            double l = vector.W;
            vector.X /= l;
            vector.Y /= l;
            vector.Z /= l;
            vector.W /= l;
        }

        public static double DotProduct(this Vector3D vector1, Vector3D vector2)
        {
            return vector1.X * vector2.X + vector1.Y * vector2.Y + vector1.Z * vector2.Z;
        }

        public static Vector3D CrossProduct(this Vector3D vector1, Vector3D vector2)
        {
            return new Vector3D(vector1.Y * vector2.Z - vector1.Z * vector2.Y, vector1.Z * vector2.X - vector1.X * vector2.Z, vector1.X * vector2.Y - vector2.X * vector1.Y);
        }

        public static double AngleCos(this Vector3D vector, Vector3D vector1)
        {
            return vector.DotProduct(vector1)/vector.Length()/vector1.Length();
        }

        public static Vector3D Symmetry(this Vector3D vector, Vector3D normal)
        {
            return (2*(vector.DotProduct(normal))*normal) - vector;
        }
    }
}
