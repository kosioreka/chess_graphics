﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Models.Primitives
{
    public class Vector3D
    {
        public Vector3D() : this(0, 0, 0) { }

        public Vector3D(double x, double y, double z, double w = 0)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double W { get; set; }

        public static implicit operator Vector3D(Vector<double> vector)
        {
            if (vector == null) throw new ArgumentNullException(nameof(vector));
            if (vector.Count < 3) return new Vector3D();
            return vector.Count == 3 ? new Vector3D(vector[0], vector[1], vector[2]) : new Vector3D(vector[0], vector[1], vector[2], vector[3]);
        }

        public static implicit operator Vector<double>(Vector3D vector)
        {
            return Vector<double>.Build.DenseOfArray(new[] { vector.X, vector.Y, vector.Z, vector.W });
        }

        public static Vector3D operator +(Vector3D vector1, Vector3D vector2)
        {
            return new Vector3D(vector1.X + vector2.X, vector1.Y + vector2.Y, vector1.Z + vector2.Z, vector1.W + vector2.W);
        }

        public static Vector3D operator -(Vector3D vector1, Vector3D vector2)
        {
            return new Vector3D(vector1.X - vector2.X, vector1.Y - vector2.Y, vector1.Z - vector2.Z, vector1.W - vector2.W);
        }

        public static Vector3D operator -(Vector3D vector)
        {
            return new Vector3D(0, 0, 0, 0) - vector;
        }

        public static Vector3D operator *(Matrix<double> matrix, Vector3D vector)
        {
            Vector<double> v = Vector<double>.Build.DenseOfArray(new double[] { vector.X, vector.Y, vector.Z, vector.W });
            Vector<double> v1 = matrix * v;
            return v1;
        }

        public Vector3D Clone()
        {
            return new Vector3D(X,Y,Z,W);
        }

        public static Vector3D operator *(double k, Vector3D vector)
        {
            return new Vector3D(k*vector.X, k*vector.Y, k*vector.Z, k*vector.W);
        }

        public static Vector3D operator /(Vector3D vector, double d)
        {
            return (1/d)*vector;
        }
    }
}
