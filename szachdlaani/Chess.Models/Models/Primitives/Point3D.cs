﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models.Models.Primitives
{
    public class Point3D
    {
        public Point3D() : this(0, 0, 0) { }

        public Point3D(double x, double y, double z, double w = 1)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double W { get; set; }

        public static implicit operator Point3D(Vector3D vector3D)
        {   
            return new Point3D(vector3D.X, vector3D.Y, vector3D.Z, vector3D.W);
        }

        public static implicit operator Vector3D(Point3D point3D)
        {
            return new Vector3D(point3D.X, point3D.Y, point3D.Z, point3D.W);
        }

        public static double Distance(Point3D p1, Point3D p2)
        {
            return ((Vector3D)p1 - (Vector3D)p2).Length();
        }

        public Point3D Clone()
        {
            return new Point3D(X,Y,Z,W);
        }
    }
}
