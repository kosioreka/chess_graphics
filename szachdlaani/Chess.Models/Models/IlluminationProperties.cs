﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Models.Models
{
    public class IlluminationProperties
    {
        public IntensityCoefficients AmbientReflections { get; set; }
        public IntensityCoefficients DiffuseReflections { get; set; }
        public IntensityCoefficients SpecularReflections { get; set; }
        public double NShine { get; set; }
    }

    public class IntensityCoefficients
    {
        public double R { get; set; }
        public double G { get; set; }
        public double B { get; set; }
    }
}
