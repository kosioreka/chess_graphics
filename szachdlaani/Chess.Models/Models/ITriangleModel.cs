﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Chess.Models.Models.Primitives;
using MathNet.Numerics.LinearAlgebra;

namespace Chess.Models.Models
{
    public interface ITriangleModel
    {
        IList<Triangle> Triangles { get; }
        Matrix<double> ModelMatrix { get; } 
        IlluminationProperties IlluminationProperties { get; }
        Color BaseColor { get; }
        void ResetModelMatrix();
    }
}
